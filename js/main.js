$(document).ready(function () {
    $("#submit").click(function(){
        $("#sort").html("");
    });

    $("#sort").sortable();
    $("#sort1").sortable();
    $("#sort2").sortable();

    $("#sort").sortable({connectWith:"#sort1, #sort2"});
});

function JavaScriptFetch() {
    var script = document.createElement('script');
    script.src = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=" + document.getElementById("search").value;
    document.querySelector('head').appendChild(script);
};


function jsonFlickrFeed(data){
    var image = "";
    data.items.forEach(function (element) {
        image += "<img  class=\"item-content\" src=\"" + element.media.m + "\"/>";
    });
    document.getElementById("sort").innerHTML = image;

};

